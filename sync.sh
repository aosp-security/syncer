#!/bin/bash

manifest_tree=https://android.googlesource.com/platform/manifest
manifest_branch=android-9.0.0_r47
manifest_file=default.xml

base_branch=android-9.0.0_r47
security_branch=android-9.0.0_r48
destination_branch=android-9.0.0_r48-r47

git clone "$manifest_tree" -b "$manifest_branch" manifest

handle_repo() {
	local repo_name
	repo_name="$1"
	my_name="$(echo "$repo_name" |tr / _)"
	rm -rf repo-"$my_name"
	git clone https://android.googlesource.com/"$repo_name" -n repo-"$my_name"
	pushd repo-"$my_name"
	(
	set -e
	git remote add aosp-security git@gitlab.com:aosp-security/"$my_name"
	git checkout "$base_branch" -b "$destination_branch"
	git merge "$security_branch"
	git push aosp-security "$destination_branch"
	) || bash

	popd
}

xmlstarlet sel -t -m '//project' -v './@name' -n manifest/default.xml |while read -r repo_name;do
	handle_repo "$repo_name"
done
